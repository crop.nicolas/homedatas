const WkHTMLToPDFController = require('./controllers/WkHTMLToPDFController')
module.exports = (app) => {
  /**
   * API
  */
  app.post(
    '/generate',
    WkHTMLToPDFController.generate
  )
  app.get(
    '/test',
    WkHTMLToPDFController.test
  )
}
