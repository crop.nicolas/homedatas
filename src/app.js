require('colors')
const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const app = express()

app.use(helmet())
app.use(cors())
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '10mb'
}))
app.use(bodyParser.json({
  limit: '10mb'
}))
app.use(morgan('combined'))

const server = require('http').Server(app)
server.listen(process.env.PORT || 8081)

require('./routes')(app)
console.log(`Application listening on port: ${process.env.PORT || 8081}`.green)
